ARG PHP_VERSION="7.4"

FROM "php:$PHP_VERSION-apache-bullseye"

RUN mkdir /app

WORKDIR /app

RUN echo "deb http://deb.debian.org/debian bullseye-backports main" >> /etc/apt/sources.list

RUN cat /etc/apt/sources.list

# Installation extensions PHP
RUN apt-get update

RUN apt-get install -y git -t bullseye-backports


RUN set -eux; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
		a2enmod headers; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libfreetype6-dev \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
		libwebp-dev \
        libzip-dev  \
        libxslt-dev \
        libxml2-dev \
        libldap2-dev \
        libuuid1  \
        uuid-dev \
	; \
	\
	docker-php-ext-configure gd --with-freetype --with-jpeg=/usr --with-webp; \
	docker-php-ext-configure soap; \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/; \
	docker-php-ext-install -j "$(nproc)" xsl bcmath intl gd opcache pdo_mysql soap zip ldap; \
	pecl install apcu uploadprogress uuid; \
	docker-php-ext-enable apcu uploadprogress uuid; \
	\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual; \
	\
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*; \
    pecl clear-cache

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

COPY --from=composer:2 /usr/bin/composer /usr/local/bin/

# Configuration apache
#RUN sed -ir "s/Listen 80/Listen 8080/g" /etc/apache2/ports.conf
RUN sed -ir "s/KeepAliveTimeout 5/KeepAliveTimeout 620/g" /etc/apache2/apache2.conf
#RUN sed -ir "s/<VirtualHost *:80>/<VirtualHost *:8080>/g" /etc/apache2/sites-available/000-default.conf
#RUN sed -ir "s/<VirtualHost *:80>/<VirtualHost *:8080>/g" /etc/apache2/sites-enabled/000-default.conf

RUN rm /etc/apache2/mods-enabled/deflate.conf
COPY conf/apache2/deflate.conf /etc/apache2/mods-enabled/deflate.conf
COPY conf/apache2/mime-webp.conf /etc/apache2/conf-enabled/mime-webp.conf

RUN echo 'memory_limit = 512M' >> $PHP_INI_DIR/conf.d/config-php.ini
RUN echo 'max_execution_time = 60' >> $PHP_INI_DIR/conf.d/config-php.ini
RUN echo 'upload_max_filesize = 40M' >> $PHP_INI_DIR/conf.d/config-php.ini
RUN echo 'max_input_vars = 2500' >> $PHP_INI_DIR/conf.d/config-php.ini
RUN echo 'post_max_size = 40M' >> $PHP_INI_DIR/conf.d/config-php.ini
RUN echo 'display_errors = off' >> $PHP_INI_DIR/conf.d/config-php.ini

RUN chown -R www-data:www-data /app;

RUN rmdir /var/www/html

RUN ln -sf /app /var/www/html

